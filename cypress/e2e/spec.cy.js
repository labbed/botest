
describe("template spec", () => {
  it("passes", () => {
    cy.visit("https://suninjuly.github.io/math.html")
    cy.get("[id=\"input_value\"]").then((xspan) => {
      let xtext = xspan.text();
      expect(xtext).to.be.a("string");
      let x = Number(xtext);
      expect(x).to.be.a("number");
      let y = Math.log(Math.abs(12 * Math.sin(x)));
      cy.get("[id=\"answer\"]").type(y.toString());
      cy.get("[id=\"robotCheckbox\"]").click();
      cy.get("[id=\"robotsRule\"]").click();
      cy.get("button[type=\"submit\"]").click();
    });
  });
});

